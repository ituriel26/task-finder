export const DEFAULT_PROPS = {
    center: {
        lat: 52.229676,
        lng: 21.012229
    },
    defaultZoom: 6.5,
    offerZoom: 13
};