export const RADIO_BUTTONS_TITLES = [
    {
        title: 'I am boss',
        value: 0,
    }, {
        title: 'I am customer',
        value: 1,
    }, {
        title: 'Just users',
        value: 2,
    }
];
export const THRESHOLD = 150;
export const END_OF_MESSAGES = 'All messages has been loaded'
export const INPUT_PLACEHOLDER = 'Input your message'