import React from 'react'
import { Icon } from 'antd';

export const ICON_MAP = {
    'Company name': <Icon type="bank"  style={{color:'black'}}/>,
    'Company size': <Icon type="team" style={{color:'red'}}/>,
    'EMP, type': <Icon type="file" />,
    'EXP, lvl': <Icon type="rise" style={{color:'green'}} />,
    'Added': <Icon type="dashboard" style={{color:'blue'}} />,
}