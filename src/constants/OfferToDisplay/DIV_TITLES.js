export const SKILL_RATE_TITLE = 'Tech req';
export const SKILL_MAP = {
    '1' : 'Student',
    '2' : 'Junior',
    '3' : 'Regular',
    '4' : 'Senior',
    '5' : 'Expert',
};
export const SKILL_INDEX_IF_UNDEFINED = '3';
export const DESCRIPTION_CARD_TITLE = 'Description';
export const APPLY_CARD_TITLE = 'Apply for the job';
