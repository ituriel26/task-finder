import {EXP_VALUES} from "../Skills/SKILLS_REQUIREMENTS";

export const TAG_COLOR = [
    "magenta",
    "red",
    "volcano",
    "orange",
    "gold",
    "lime",
    "green",
    "cyan",
    "blue",
    "geekblue",
    "purple",
];

export const EXP_COLOR_MAP = EXP_VALUES.reduce((map, exp, index) => {
    return map.set(exp, TAG_COLOR[index + 3]);
}, new Map());
