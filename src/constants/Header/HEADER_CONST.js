export const TITLE = 'todo.id';
export const HEADER_ICONS = [
    {
        type: 'code',
        title: 'Job offers',
    },
    {
        type: 'project',
        title: 'Brand Stories',
    },
    {
        type: 'desktop',
        title: 'Just Geek It',
    }
]
export const HEADER_DRAWER = {
    buttonSize:'large',
    title: 'Todo.it',
    drawerItems: [
        {
            to: '/chat',
            type: 'message',
            title: 'Chat', 
        },
        {
            to: '/',
            type: 'code',
            title: 'Employer Panel',
        }, {
            to: '/',
            type: 'facebook',
            title: 'Todo.it',
        }, {
            to: '/',
            type: 'team',
            title: 'Event',
        }, {
            to: '/',
            type: 'audio',
            title: 'About us',
        }, {
            to: '/',
            type: 'wifi',
            title: 'RSS',
        }, {
            to: '/',
            type: 'file-pdf',
            title: 'Terms',
        },{
            to: '/',
            type: 'file-pdf',
            title: 'Policy',
        },]
}