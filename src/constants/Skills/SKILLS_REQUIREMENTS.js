export const SELECT_SALARY_TITLE = 'Salary';
export const SELECT_EXP_TITLE = 'Exp';
export const SALARY_VALUES = ['All', '0 - 5000', '5000- 10000', '10000 - 15000', '15000+'];
export const EXP_VALUES = ['All', 'Student', 'Junior', 'Mid', 'Senior'];