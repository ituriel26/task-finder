import { SAVE_TOKEN, SAVE_USER, LOG_IN } from "../actionTypes";

export const saveToken = content => ({
    type: SAVE_TOKEN,
    payload: content
});

export const saveUser = content => ({
    type: SAVE_USER,
    payload: content
});

export const logIn = () => ({
    type: LOG_IN,
});