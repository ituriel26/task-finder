import { HIDE_MODAL, SHOW_MODAL } from "../actionTypes";

export const showModal = content => ({
    type: SHOW_MODAL,
    payload: content
});

export const hideModal = () => ({
    type: HIDE_MODAL,
});
