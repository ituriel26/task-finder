import {HIDE_MODAL, SHOW_MODAL} from "../actionTypes";

const initialState = {
    visible: false,
    content: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SHOW_MODAL: {
            return {
                visible: true,
                content: action.payload
            };
        }
        case HIDE_MODAL: {
            return {
                visible: false,
                content: {}
            };
        }
        default:
            return state;
    }
};