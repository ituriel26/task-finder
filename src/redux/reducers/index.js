import { combineReducers } from "redux";
import modalReducers from './modalRecuders';
import userReducers from './userReducers';

export default combineReducers({ modalReducers, userReducers });