import { SAVE_TOKEN, SAVE_USER, LOG_IN } from "../actionTypes";

const initialState = {
    token: '',
    user: {},
    loggedIn: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SAVE_TOKEN: {
            return {
                ...state,
                token: action.payload,
            };
        }
        case SAVE_USER: {
            return {
                ...state,
                user: action.payload,
            };
        }
        case LOG_IN: {
            return {
                ...state,
                loggedIn: true,
            };
        }
        default:
            return state;
    }
};