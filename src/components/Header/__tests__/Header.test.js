import React from 'react';
import {shallow} from 'enzyme'
import Header, {HeaderLeft, HeaderRight, HeaderIcon} from "../Header";

describe('Header component test', () => {
    let wrapper;
    it('Check if all subcomponent rendered', () => {
        wrapper = shallow(<Header/>);
        expect(wrapper.find(HeaderLeft).length).toBe(1);
        expect(wrapper.find(HeaderRight).length).toBe(1);
        expect(wrapper.find(HeaderIcon).length).toBe(1);
    });
});