import React from 'react';
import styled from 'styled-components';

import HeaderRight from './HeaderRight';
import { HeaderLeft } from './HeaderLeft';
import { HeaderIcon } from './HeaderIcon';

const HeaderDiv = styled.div`
    display: flex;
    min-width: 100%;
    align-items: center;
    height: 10%;
    margin-top: 10px;
    margin-bottom: 20px;
`;

const HeaderItem = styled.div`
    flex: 1;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: ${props => props.justifyContent ?props.justifyContent : 'flex-start'};
`;

const Header = () => {
    return (
        <HeaderDiv>
            <HeaderItem>
                <HeaderIcon/>
                <HeaderLeft/>
            </HeaderItem>
            <HeaderItem justifyContent={'flex-end'}>
                <HeaderRight/>
            </HeaderItem>
        </HeaderDiv>
    )
};

export default Header;