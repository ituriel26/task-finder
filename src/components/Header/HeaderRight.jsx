import React, { useState } from 'react'
import styled from 'styled-components';
import { connect } from "react-redux";
import { Link } from 'react-router-dom'
import { Button, Drawer, Icon } from "antd";

import SocialMediaModal from '../Modal/SocialMediaModal/SocialMediaModal';
import { showModal, hideModal } from '../../redux/actions/modalActions';
import { HEADER_DRAWER } from '../../constants/Header/HEADER_CONST';
import { MODAL_TITLE } from '../../constants/Modal/SocialMedia/SOCIALMEDIA_MODAL'

const HeaderButton = styled(Button)`
    margin-right: 10px; 
`;

const DrawerLink = styled(Link)`
    display: flex;
    flex-basis: auto;
    align-items: center;
    margin: 20px;
`;

const HeaderIcon = styled(Icon)`
    margin-right: 10px; 
    font-size: 30px;
`;

const HeaderMenuIcon = styled(Icon)`
    margin: 20px; 
`;

const HeaderRight = ({ loggedIn, showModal, hideModal }) => {
    const [visible, setVisible] = useState(false);

    return (
        <>
            {loggedIn ?
                <HeaderButton
                    type="primary"
                    size={HEADER_DRAWER.buttonSize} >
                    Post a Job
                 </HeaderButton>
                :
                <HeaderButton
                    type="danger"
                    size={HEADER_DRAWER.buttonSize}
                    onClick={() => showModal({ title: MODAL_TITLE, content: <SocialMediaModal hideModal={hideModal} /> })}>
                    Sign in
                </HeaderButton>
            }
            <div>
                <HeaderMenuIcon onClick={() => setVisible(!visible)} type="menu" />
                <Drawer
                    title={HEADER_DRAWER.title}
                    placement="right"
                    closable={true}
                    onClose={() => setVisible(!visible)}
                    visible={visible}
                >
                    {HEADER_DRAWER.drawerItems.map(item => {
                        return (
                            <DrawerLink key={item.title} to={item.to}>
                                <HeaderIcon type={item.type} />
                                <span>{item.title}</span>
                            </DrawerLink>
                        )
                    })}

                </Drawer>
            </div>
        </>
    )
};


const actionToProps = dispatch => {
    return {
        showModal: (content) => dispatch(showModal(content)),
        hideModal: () => dispatch(hideModal())
    }
};

const stateToProps = state => {
    return {
        loggedIn: state.userReducers.loggedIn
    }
}

export default connect(stateToProps, actionToProps)(HeaderRight)