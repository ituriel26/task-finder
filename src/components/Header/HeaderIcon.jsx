import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom'

import { TITLE } from '../../constants/Header/HEADER_CONST';

const LogoIconP = styled.p`
    margin: 20px;
    font-size: 20px;
`;

export const HeaderIcon = () => {
    return (
        <Link to={{ pathname: '/' }}>
            <LogoIconP>{TITLE}</LogoIconP>
        </Link >
    )
}
