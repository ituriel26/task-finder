import React from 'react'
import { Icon } from "antd";
import styled from 'styled-components';

import { HEADER_ICONS } from '../../constants/Header/HEADER_CONST';

const HeaderA = styled.a`
    display: flex;
    flex-basis: auto;
    align-items: center;
    margin: 20px;
`;

const HeaderIcon = styled(Icon)`
    margin-right: 10px; 
    font-size: 30px;
`;

export const HeaderLeft = () => {
    return (
        <>
            {HEADER_ICONS.map(headerIcon => {
                return (
                    <HeaderA key={headerIcon.title}>
                        <HeaderIcon type={headerIcon.type}/>
                        <span>{headerIcon.title}</span>
                    </HeaderA>
                )
            })}
        </>
    )
};