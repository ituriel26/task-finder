import React from 'react'
import styled from 'styled-components';

import Map from 'pigeon-maps';
import Marker from 'pigeon-marker'

import { DEFAULT_PROPS } from '../../constants/Map/MAP_CONSTANTS'

const MapDiv = styled.div`
    width: 50%;
    height: 80vh;
`;

const OfferMap = props => {
    const { offerToDisplay, hoveredOffer } = props;

    const center = chooseCenter(offerToDisplay);
    const zoom = chooseZoom(offerToDisplay);
    const marker = createMarker(hoveredOffer);

    return (
        <MapDiv>
            <Map center={center} zoom={zoom}>
                {marker}
            </Map>
        </MapDiv>
    )
}

const createMarker = selected => {
    return selected && Object.keys(selected).length !== 0 ?
        <Marker anchor={[selected.lat, selected.lng]} payload={selected.id} /> :
        null;
};

const chooseCenter = offerToDisplay => {
    return !offerToDisplay || Object.keys(offerToDisplay).length === 0 ?
        [DEFAULT_PROPS.center.lat, DEFAULT_PROPS.center.lng] :
        [offerToDisplay.lat, offerToDisplay.lng]
};

const chooseZoom = offerToDisplay => {
    return !offerToDisplay || Object.keys(offerToDisplay).length === 0 ?
        DEFAULT_PROPS.defaultZoom :
        DEFAULT_PROPS.offerZoom;
};

export default OfferMap