import React from 'react'
import styled from 'styled-components'
import InfiniteScroll from 'react-infinite-scroller';
import uuid from 'uuid'

import UserLoading from './UserLoading'
import SingleUser from './SingleUser'

const UserUl = styled.ul`
    flex : 1.5;
    display: flex;
    flex-direction: column;
    height: 90vh;
    padding: 10px 10px 10px 10px;
    overflow: auto;
    border-top: solid 1px #aaaaaa;
`;

const UserList = ({ fetchMoreUserList, setSelectedChat, selectedChat, userList, chatRadio, hasMore }) => {
    return (
        <UserUl>
            <InfiniteScroll
                useWindow={false}
                pageStart={0}
                loadMore={() => fetchMoreUserList()}
                loader={<UserLoading key={uuid()} />}
                hasMore={hasMore}>
                {chatRadio}
                {userList && userList
                    .map(singleUser => {
                        const propsToSingleUser = {
                            ...singleUser,
                            setSelectedChat,
                            isSelected: singleUser.chatId === selectedChat
                        }
                        return <SingleUser key={singleUser.username} {...propsToSingleUser} />
                    })}
            </InfiniteScroll>
        </UserUl>
    )
}

export default UserList
