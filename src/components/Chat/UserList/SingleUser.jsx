import React from 'react'
import styled from 'styled-components'
import { Avatar } from 'antd';

const UserLi = styled.li`
    display: flex;
    justify-content: flex-start;
    height: 10vh;
    border-radius: 10px;
    align-items: center;
    padding-left: 10px;
    margin-bottom: 5px;
    background-color : ${props=>props.isSelected ?  'rgba(255, 0, 0, .1)' : null };
    &:hover {
        background-color : ${props=>props.isSelected ?  'rgba(255, 0, 0, .1)' : ' rgba(0, 0, 0, .05)' };
    }
`;

const Span = styled.span`
    max-width: ${props => props.width ? props.width : '250px'};
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

const UserDataDiv = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding-left: 16px;
`;

const SingleUser = ({ isSelected, avatarUrl, username, lastMessage, date, setSelectedChat, chatId }) => {
    return (
        <UserLi
            isSelected={isSelected}
            onClick={() =>setSelectedChat(chatId)}>
            <Avatar src={avatarUrl} size={50} icon="user" />
            <UserDataDiv>
                <Span>{username}</Span>
            </UserDataDiv>
        </UserLi>
    )
}

export default SingleUser
