import React from 'react'
import styled from 'styled-components'
import { Radio } from 'antd';

import { RADIO_BUTTONS_TITLES } from '../../../constants/Chat/CHAT_CONST'

const { Group, Button } = Radio;

const RadioGroup = styled(Group)`
    && { 
    flex:1;
    height: 5vh;
    display:flex;
    margin-bottom: 10px;
    }
`;

const RadioButton = styled(Button)`
    && { 
    text-align: center;
    }
`;

const ChatRadio = ({ setRadio, setSelectedChat }) => {
    return (
        <RadioGroup
            onChange={e => {
                setSelectedChat(0)
                setRadio(e.target.value)
            }}
            defaultValue={0}>
            {RADIO_BUTTONS_TITLES.map(({ value, title }) =>
                <RadioButton key={value} value={value}>{title}</RadioButton>
            )}
        </RadioGroup>
    )
}

export default ChatRadio
