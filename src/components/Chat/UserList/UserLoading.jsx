import React from 'react'
import styled from 'styled-components'
import { Avatar, Spin } from 'antd';

const UserLi = styled.li`
    display: flex;
    justify-content: flex-start;
    height: 10vh;
    border-radius: 10px;
    align-items: center;
    padding-left: 10px;
    margin-bottom: 5px;
    margin-right: 16px;
`;

const UserDataDiv = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    padding-left: 16px;
`;

const SingleUser = () => {
    return (
        <>
            <UserLi >
                <Avatar size={50} icon="user" />
                <UserDataDiv>
                    <Spin size="small" />
                    <Spin size="small" />
                </UserDataDiv>
            </UserLi>
            <UserLi>
                <Avatar size={50} icon="user" />
                <UserDataDiv>
                    <Spin size="small" />
                    <Spin size="small" />
                </UserDataDiv>
            </UserLi>
        </>
    )
}

export default SingleUser
