import React from 'react'
import styled from 'styled-components'
import { Avatar } from 'antd'

const ChatHeaderDiv = styled.div`
    padding-left: 64px;
    display: flex;
    height: 12vh;
    width: 100%;
    border-bottom: solid 1px #aaaaaa;
    text-align: center;
`;

const AvatarHeader = styled(Avatar)`
    && {
    margin: auto 0;
    }
`;

const Span = styled.span`
    font-size: 36px;
    padding-left: 64px;
    margin: auto 0;
    text-align: center;
`;

const ChatHeader = ({ name, url }) => {
    return (
        <ChatHeaderDiv>
            <AvatarHeader src={url} size={64} icon="user" />
            <Span>{name}</Span>
        </ChatHeaderDiv>
    )
}

export default ChatHeader