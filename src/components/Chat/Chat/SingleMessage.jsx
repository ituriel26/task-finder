import React from 'react'
import styled from 'styled-components'

import { Avatar } from 'antd'

const MessageDiv = styled.div`
    flex:1;
    display: flex;
    align-self : ${props => props.self ? 'flex-end' : 'flex-start'};
    flex-direction : ${props => props.self ? 'row-reverse' : 'row'};
    margin-left : ${props => props.self ? '0px' : '36px'};
    margin-right :  ${props => props.self ? '36px' : '0px'};
    margin-bottom: 16px;
    align-items: center;
`;

const MessageContentDiv = styled.div`
    flex:1;
    display: flex;
    flex-direction : column;
    margin: 5px;
`;

const H5 = styled.h5`
    font-size: 10px;
`;

const Message = styled.span`
    border-radius: 10px;
    background-color: rgba(0, 0, 0, .05);
    padding: 5px;
`;

const SingleMessage = (props) => {
    const {
        avatarUrl,
        date,
        message,
        self,
        username,
    } = props;

    return (
        <MessageDiv self={self}>
            <Avatar src={avatarUrl} size={36} icon="user" />
            <MessageContentDiv self={self} >
                <H5>{username}</H5>
                <Message>{message}</Message>
            </MessageContentDiv>
            <h6>{date}</h6>
        </MessageDiv>
    )
}

export default SingleMessage
