import React from 'react'
import styled from 'styled-components'

import { Avatar, Spin } from 'antd'

const MessageDiv = styled.div`
    flex:1;
    display: flex;
    align-self : ${props => props.self ? 'flex-end' : 'flex-start'};
    flex-direction : ${props => props.self ? 'row-reverse' : 'row'};
    margin-left : ${props => props.self ? '0px' : '36px'};
    margin-right :  ${props => props.self ? '36px' : '0px'};
    margin-bottom: 16px;
    align-items: center;
`;

const MessageContentDiv = styled.div`
    flex:1;
    display: flex;
    flex-direction : column;
    margin: 5px;
`;

const Message = styled.span`
    border-radius: 10px;
    background-color: rgba(0, 0, 0, .05);
    padding: 5px;
`;

const SingleMessageLoader = () => {
    return (
        <>
            <MessageDiv self={true}>
                <Avatar  size={36} icon="user" />
                <MessageContentDiv self={true} >
                    <Message><Spin/></Message>
                </MessageContentDiv>
            </MessageDiv>
            <MessageDiv self={false}>
                <Avatar size={36} icon="user" />
                <MessageContentDiv self={false} >
                <Message><Spin/></Message>
                </MessageContentDiv>
            </MessageDiv>
            <MessageDiv self={true}>
                <Avatar  size={36} icon="user" />
                <MessageContentDiv self={true} >
                    <Message><Spin/></Message>
                </MessageContentDiv>
            </MessageDiv>
        </>
    )
}

export default SingleMessageLoader
