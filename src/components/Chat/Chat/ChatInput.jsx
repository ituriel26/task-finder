import React, { useState } from 'react';
import { connect } from 'react-redux'
import styled from 'styled-components'
import uuid from 'uuid'

import { Input, Icon } from 'antd'
import { INPUT_PLACEHOLDER } from '../../../constants/Chat/CHAT_CONST'

const InputDiv = styled.div`
    flex: 1; 
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const ArrowIcon = styled(Icon)`
    font-size: 36px;
`;

const ChatInput = ({ user, chatId, addMessage, setScrollFlab, scrollFlag }) => {
    const [value, setValue] = useState('');

    const sendMessage = () => {
        if (value !== '') {
            addMessage(chatId, {
                id: uuid(),
                avatarUrl: user.imageUrl,
                username: user.name,
                message: value,
                date: new Date().toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric' })
            })
            setValue('')
            setScrollFlab(!scrollFlag)
        }
    }

    return (
        <InputDiv>
            <Input
                placeholder={INPUT_PLACEHOLDER}
                value={value}
                onChange={(e) => setValue(e.target.value)}
                onPressEnter={() => sendMessage()} />
            <ArrowIcon
                onClick={() => sendMessage()}
                type="arrow-right" />
        </InputDiv>
    );
}

const stateToProps = state => {
    return {
        user: state.userReducers.user,
    };
};


export default connect(stateToProps, null)(ChatInput);