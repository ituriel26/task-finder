import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'
import InfiniteScroll from 'react-infinite-scroller';
import uuid from 'uuid'

import SingleMessage from './SingleMessage'
import ChatHeader from './ChatHeader'
import SingleMessageLoader from './SingleMessageLoader'
import ChatInput from './ChatInput'

import { THRESHOLD, END_OF_MESSAGES } from '../../../constants/Chat/CHAT_CONST'

const InfiniteScrollDiv = styled.div`
    flex : 4;
    display: flex;
    flex-direction: column;
    height: 90vh;
    border: solid 1px #aaaaaa;
    overflow: auto;
`;

const Span = styled.span`
    font-size: 50px;
    margin: auto 0;
    text-align: center;
`;

const Chat = ({ headerData, selfName, chatId, chatHistoryMap, fetchMoreChatHistory, addMessage }) => {
    const chatRef = useRef(null)
    const hasMoreChatHistory = chatHistoryMap && chatHistoryMap['hasMore'];
    const chatHistory = chatHistoryMap && chatHistoryMap['history'];
    const [scrollFlag, setScrollFlab] = useState(false)

    useEffect(() => chatRef.current.scrollIntoView({ behavior: "smooth" }), [scrollFlag]);

    return (
        <InfiniteScrollDiv >
            {chatHistory && headerData && <ChatHeader {...headerData} />}
            {hasMoreChatHistory === false && <Span>{END_OF_MESSAGES}</Span>}
            <InfiniteScroll
                isReverse={true}
                useWindow={false}
                pageStart={0}
                threshold={THRESHOLD}
                loader={chatId !== 0 && <SingleMessageLoader key={uuid()} />}
                loadMore={() => chatId !== 0 ? fetchMoreChatHistory(chatId) : null}
                hasMore={hasMoreChatHistory === undefined ? true : hasMoreChatHistory}>
                {chatHistory && chatHistory.map(singleMessage => {
                    const propsToSingleMessage = {
                        ...singleMessage,
                        self: singleMessage.username === selfName
                    }
                    return <SingleMessage key={singleMessage.id} {...propsToSingleMessage} />
                })}
                <div ref={chatRef} />
                {chatHistory && <ChatInput addMessage={addMessage} chatId={chatId} scrollFlag={scrollFlag} setScrollFlab={setScrollFlab} />}
            </InfiniteScroll>
        </InfiniteScrollDiv>
    )
}

export default Chat
