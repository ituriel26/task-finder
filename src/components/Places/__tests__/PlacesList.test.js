import React from 'react';
import {shallow} from 'enzyme'
import {Radio} from "antd";
import configureStore from 'redux-mock-store'

import PlacesListConnected, {PlacesList, showDefaultPlaces} from "../PlacesList";
import SelectOtherPlace from "../SelectOtherPlace";

const RadioGroup = Radio.Group;
const RadioButton = Radio.Button;

const initialState = {
    placeReducers: {
        selectedPlace: 'All',
        selectedOtherPlace: '',
    }
};
const middlewares = [];
const mockStore = configureStore(middlewares);
const OTHER_CITY = 'Kielce';
const OTHER_CITY_INCORECT = 'Kielcee';

describe('Places component test', () => {

    describe('PlaceList render tests', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = shallow(<PlacesList/>);
        });

        it('Check if all subcomponent rendered', () => {
            expect(wrapper.find(SelectOtherPlace).length).toBe(1);
            expect(wrapper.find(RadioGroup).length).toBe(1);
            expect(wrapper.find(RadioButton).length).toBe(8);
        });

        it('Check if all subcomponent rendered', () => {
            wrapper = shallow(<PlacesList selectedOtherPlace={OTHER_CITY}/>);
            expect(wrapper.find(SelectOtherPlace).length).toBe(1);
            expect(wrapper.find(RadioGroup).length).toBe(1);
            expect(wrapper.find(RadioButton).length).toBe(9);
        });
    });
    describe('PlaceList unit tests', () => {
        const result1 = showDefaultPlaces();
        const result2 = showDefaultPlaces(OTHER_CITY);
        const result3 = showDefaultPlaces(OTHER_CITY_INCORECT);

        it('showDefaultPlaces, empty input', () => {
            expect(result1.length).toBe(8);
        });
        it('showDefaultPlaces correct input', () => {
            expect(result2.length).toBe(9);
        });
        it('showDefaultPlaces incorrect input', () => {
            expect(result3.length).toBe(8);
        });
    });
    describe('PlaceListConnected tests', () => {
        let wrapper;
        let store;

        beforeEach(() => {
            store = mockStore(initialState);
            wrapper = shallow(<PlacesListConnected store={store}/>);
        });

        it('Check PlacesList Connected render ', () => {
            expect(wrapper.length).toEqual(1)
        });
        it('Check PlacesList Connected props ', () => {
            const props = wrapper.prop('children').props;
            expect(props['selectedPlace']).toEqual(initialState.placeReducers.selectedPlace);
            expect(props['selectedOtherPlace']).toEqual(initialState.placeReducers.selectedOtherPlace);
        });
    });

});
