import React from 'react';
import {shallow} from 'enzyme'

import Places from "../Places";
import ThemeSwitch from "../ThemeSwitch";
import PlacesList from "../PlacesList";

describe('Places component test', () => {
    let wrapper;
    it('Check if all subcomponent rendered', () => {
        wrapper = shallow(<Places/>);
        expect(wrapper.find(PlacesList).length).toBe(1);
        expect(wrapper.find(ThemeSwitch).length).toBe(1);
    });
});