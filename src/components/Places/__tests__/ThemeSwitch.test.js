import React from 'react';
import {shallow} from 'enzyme'
import {Icon, Switch} from "antd";
import configureStore from "redux-mock-store";

import ThemeSwitchConnected, {ThemeSwitch} from "../ThemeSwitch";

const initialState = {
};
const middlewares = [];
const mockStore = configureStore(middlewares);

describe('ThemeSwitch component test', () => {
    let wrapper;
    it('Check if all subcomponent rendered', () => {
        wrapper = shallow(<ThemeSwitch/>);
        expect(wrapper.find(Icon).length).toBe(2);
        expect(wrapper.find(Switch).length).toBe(1);
    });

    describe('ThemeSwitchConnected component test',()=>{
        let wrapper;
        let store;

        beforeEach(() => {
            store = mockStore(initialState);
            wrapper = shallow(<ThemeSwitchConnected store={store}/>);
        });
        it('Check ThemeSwitchConnected render ', () => {
            expect(wrapper.length).toEqual(1)
        });
    })
});