import React from 'react'
import styled from 'styled-components'
import { Radio } from "antd";

import { DEFAULT_CITIES } from "../../constants/Places/CITIES";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

const PlacesListDiv = styled.div`
    display: flex; 
`;

export const PlacesList = ({ setOptionsFilter, optionsFilter }) => {
    const { place } = optionsFilter;

    const onChange = ({ target }) => {
        const { value } = target;
        const copy = Object.assign({}, optionsFilter, { place: value })
        setOptionsFilter(copy)
    }

    return (
        <PlacesListDiv>
            <RadioGroup
                value={place}
                onChange={onChange}
                size="medium"
                buttonStyle="solid"
                defaultValue="All">
                {DEFAULT_CITIES
                    .map(city => (
                        <RadioButton key={city} value={city}>
                            {city}
                        </RadioButton>
                    ))}
            </RadioGroup>
        </PlacesListDiv>
    )
};

export default PlacesList