import React from 'react';
import styled from 'styled-components'

import PlacesList from "./PlacesList";

const PlacesDiv = styled.div`
    display: flex; 
    padding-left: 16px;
    min-width: 100%;
`;

const Places =  ({ setOptionsFilter, optionsFilter }) => {
    const propsToSkills = {
        setOptionsFilter,
        optionsFilter
    }
    return (
        <PlacesDiv>
            <PlacesList {...propsToSkills}/>
        </PlacesDiv>
    )
};

export default Places;
