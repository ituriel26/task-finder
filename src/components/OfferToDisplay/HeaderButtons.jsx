import React from 'react'
import styled from 'styled-components';
import { Icon as icon } from 'antd'

const BackAndShareDiv = styled.div`
    display: flex;
    justify-content: row;
`;

const IconDiv = styled.a`
    flex:1;
    display: flex; 
    justify-content: ${props => props.jusifyContent ? props.jusifyContent : 'flex-start'};
    font-size: 16px;
    margin-left: ${props => props.marginLeft ? props.marginLeft : '0px'};
    margin-right: ${props => props.marginRight ? props.marginRight : '0px'};
    margin-right: 25px;
    margin-top: 9px;
`;

const Icon = styled(icon)`
    padding: 5px;
    border-radius: 5px;
    color: white;
    background-color: rgba(0,0,0,0.2);
    :hover {
        background-color: rgba(0,0,0,0.1);
    }
`;


export const HeaderButtons = ({ clearOfferDetails }) => {
    return (
        <BackAndShareDiv>
            <IconDiv marginLeft={'16px'}>
                <Icon type="left" onClick={() => clearOfferDetails()} />
            </IconDiv>
            <IconDiv marginRight={'25px'} jusifyContent={'flex-end'}>
                <Icon type="share-alt" />
            </IconDiv>
        </BackAndShareDiv>
    )
}

export default HeaderButtons;