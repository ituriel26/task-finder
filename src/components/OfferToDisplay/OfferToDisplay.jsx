import React from 'react';
import styled from 'styled-components';
import { Card as card } from 'antd'

import HeaderButtons from './HeaderButtons';
import OfferIcon from './OfferIcon';
import GeneralOfferDetails from './GeneralOfferDetails'
import ContactButton from './ContactButton'
import SkillsRate from './SkillsRate'
import GeneralOfferCardDetails from './GeneralOfferCardDetails'
import ApplyForm from './ApplyForm'

import { DESCRIPTION_CARD_TITLE, APPLY_CARD_TITLE } from '../../constants/OfferToDisplay/DIV_TITLES'


const OfferToDisplayDiv = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    width: 50%;
    height: 100%;
    padding-right: 20px;
    padding-left: 20px;
    overflow: auto;
    background-color: #f5fbff;
`;

const OfferHeaderDiv = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    background: linear-gradient(to right, #ffefba, #ffffff);
    border-radius: 1px;
    padding-bottom: 20px;
`;

const Div = styled.div`
    display: flex;
    flex: 1;
    padding-bottom: ${props => props.paddingBottom ? props.paddingBottom : '0px'};
`;

const Card = styled(card)`
    width: ${props => props.width ? props.width : "100%"};
`;

export const OfferToDisplay = ({ offerToDisplay, clearOfferDetails }) => {
    const {
        id,
        skills,
        companyDetails,
        src,
        salary,
        title,
        adress,
        sampleDescription,
    } = offerToDisplay;

    return (
        <OfferToDisplayDiv>
            <OfferHeaderDiv>
                <HeaderButtons clearOfferDetails={clearOfferDetails} />
                <Div>
                    <OfferIcon src={src} />
                    <GeneralOfferDetails salary={salary} title={title} adress={adress} />
                    <ContactButton />
                </Div>
            </OfferHeaderDiv>
            <GeneralOfferCardDetails cardDetails={companyDetails} />
            <SkillsRate skills={skills} />
            <Div paddingBottom={'16px'}>
                <Card title={DESCRIPTION_CARD_TITLE}>
                    {sampleDescription}
                </Card>
            </Div>
            <Div>
                <Card title={APPLY_CARD_TITLE}>
                    <ApplyForm />
                </Card>
            </Div>
        </OfferToDisplayDiv>
    )
}

export default OfferToDisplay