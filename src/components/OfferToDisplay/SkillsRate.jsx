import React from 'react'
import styled from 'styled-components';
import { Card, Rate } from 'antd';

import { SKILL_RATE_TITLE, SKILL_MAP, SKILL_INDEX_IF_UNDEFINED } from '../../constants/OfferToDisplay/DIV_TITLES'

const Flex = styled.div`
    display: flex;
`;

const SkillsRateDiv = styled(Flex)`
    margin-bottom: 20px;
`;

const SkillCard = styled(Card)`
    flex: 1;
    && {
    border-radius: 5px;
    }
`;

const FlexWrapDiv = styled(Flex)`
    flex-wrap: wrap;
`;

const SingleSkillDiv = styled.div`
    flex: 1;
    justify-content: center;
    max-width: 20%;
    padding-bottom: 16px;
    padding-left: 5px;
    padding-right: 5px;
    flex-basis: 20%;
`;

const SkillRateWrapper = styled(Rate)`
    && {
    font-size: ${props => props.fontSize};
    justify-content: 'center';
    padding-bottom: '5px'
    }
`;

const SkillSpan = styled.span`
    flex: 1;
    display: block;
    font-size: ${props => props.fontSize};
`;

const SkillRate = ({ defaultValue }) => {
    return <SkillRateWrapper fontSize={'12px'} defaultValue={defaultValue} disabled />
}

export const SkillsRate = (props) => {
    const { skills } = props;

    return (
        <SkillsRateDiv>
            <SkillCard title={SKILL_RATE_TITLE} bordered={false}>
                <FlexWrapDiv >
                    {skills && skills.map(singleSkillCard)}
                </FlexWrapDiv>
            </SkillCard>
        </SkillsRateDiv>
    )
}

const singleSkillCard = (skillDetails) => {
    return (
        <SingleSkillDiv key={skillDetails.skillName}>
            <SkillRate defaultValue={skillDetails.stars} />
            <SkillSpan fontSize={'12px'}>{skillDetails.skillName}</SkillSpan>
            <SkillSpan fontSize={'16px'}>{SKILL_MAP[skillDetails.stars] ? SKILL_MAP[skillDetails.stars] : SKILL_MAP[SKILL_INDEX_IF_UNDEFINED]}</SkillSpan>
        </SingleSkillDiv>
    )
}

export default SkillsRate