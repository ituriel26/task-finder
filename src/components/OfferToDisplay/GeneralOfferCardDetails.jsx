import React from 'react';
import styled from 'styled-components';
import { Card } from 'antd';

import { ICON_MAP } from '../../constants/OfferToDisplay/ICON_MAP'

const Flex = styled.div`
    flex: 1;
    display: flex;
`;

const GeneralOfferCardDetailsDiv = styled(Flex)`
    margin-bottom: 20px;
    margin-top: 20px;
    border-radius: 5px;
    max-height: 160px;
`;

const SingleCard = styled(Card)`
    flex-basis: 20%;
    text-align: center;
    && {
    margin-right: 16px;
    :last-child {
    margin-right: 0px; 
    }}
`;

const P = styled.p`
    color: ${props => props.color ? props.color : 'black'};
    font-size: ${props => props.size ? props.size : '9px'};
`;

const A = styled.a`
    color: ${props => props.color ? props.color : 'black'};
    font-size: ${props => props.size ? props.size : '9px'};
    display: block;
    margin-block-end: 1em;
`;

const showTitle = ({ ahref, title }) => {
    return ahref ?
        <A href={ahref} size={'15px'} color={'blue'}>{title}</A> :
        <P size={'15px'}>{title}</P>;
}

export const GeneralOfferCardDetails = ({ cardDetails }) => {
    return (
        <GeneralOfferCardDetailsDiv>
            {cardDetails && cardDetails.map(singleCard => {
                return (
                    <SingleCard key={singleCard.title} title={ICON_MAP[singleCard.details]}>
                        {showTitle(singleCard)}
                        <P size={'12px'} color={'#bababf'}>{singleCard.details}</P>
                    </SingleCard>
                )
            })}
        </GeneralOfferCardDetailsDiv>
    )
}

export default GeneralOfferCardDetails;

