import React from 'react'
import styled from 'styled-components'
import { Button } from 'antd';

const ContactButtonDiv = styled.div`
    display: flex;
    flex: 1;
    flex-direction: row-reverse;
    align-items: center;
    margin-right: 16px; 
`;

export const ContactButton = () => {
    return (
        <ContactButtonDiv>
            <Button type="primary" shape="round" size={'large'}>
                Contact
            </Button>
        </ContactButtonDiv>
    )
};

export default ContactButton;