import React from 'react'
import styled from 'styled-components'

const GeneralDiv = styled.div`
    flex: 3;
    flex-direction: row;
    padding-top: 16px;
    text-align: left;
    padding-top: 16px;
    padding-left: 100px;
`;
const Span = styled.span`
    flex: 1;
    display: block;
`;

const BorderSpan = styled(Span)`
    font-size: 16px;
`;

const CenterSpan = styled(Span)`
    font-size: 25px;
`;

export const GeneralOfferDetails = ({ salary, title, adress }) => {
    return (
        <GeneralDiv>
            <BorderSpan >{salary}</BorderSpan>
            <CenterSpan >{title}</CenterSpan>
            <BorderSpan >{adress}</BorderSpan>
        </GeneralDiv>
    )
}

export default GeneralOfferDetails