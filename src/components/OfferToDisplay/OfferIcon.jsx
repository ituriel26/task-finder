import React from 'react'
import styled from 'styled-components'
import { Avatar } from 'antd';

const OfferIconDiv = styled.div`
    flex: 1;
    text-align: center;
    padding-top: 16px;
    padding-left: 30px;
`;

export const OfferIcon = ({ src }) => {
    return (
        <OfferIconDiv>
            <Avatar
                size={100}
                icon="user"
                src={src} />
        </OfferIconDiv>
    )
}

export default OfferIcon;