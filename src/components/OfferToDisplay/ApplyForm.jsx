import React from 'react'
import styled from 'styled-components'
import { message, Form, Icon, Input, Button, Upload } from 'antd'

const ButtonDiv = styled.div`
    flex: 1;
    display: flex; 
    justify-content: flex-end;
    height: 32px;
`;

const TextAreaAndUploadDiv = styled.div`
    flex-basis: 100%;
`;

const FormStyled = styled(Form)`
    margin-bottom: 16px;
    display: flex;
    flex-wrap: wrap;
`;

const FormItem = styled(Form.Item)`
    text-align: ${props => props.textAlign ? props.textAlign : 'inherit'};
    flex-basis: 50%;
`;

const TransparentIvon = styled(Icon)`
    && {
        color: ${props => props.color ? props.color : "rgba(0,0,0,.25)"};
    }
`;

const normFile = e => {
    if (Array.isArray(e)) {
        return e;
    }
    return e && e.fileList;
};

const onSubmit = (form, e) => {
    e.preventDefault();
    form.validateFields(err => {
        if (!err) {
            message.success('Form completed correctly!');
        }
        else {
            message.error('Check form input!');
        }
    })
}

export const ApplyForm = ({ form }) => {
    const { getFieldDecorator } = form;
    return (
        <FormStyled onSubmit={e => onSubmit(form, e)}>
            <FormItem>
                {getFieldDecorator('nameAndSurname', {
                    rules: [{ required: true, message: 'Please input your name & surname!' }],
                })(
                    <Input
                        prefix={<TransparentIvon type="user" />}
                        placeholder="First & last name *"
                    />,
                )}
            </FormItem>
            <FormItem>
                {getFieldDecorator('email', {
                    rules: [{ required: true, message: 'Please input your email' }],
                })(
                    <Input
                        type="email"
                        prefix={<TransparentIvon type="mail" />}
                        placeholder="Email *"
                    />,
                )}
            </FormItem>
            <TextAreaAndUploadDiv>
                <FormItem>
                    {getFieldDecorator('details', {
                        rules: [{ required: true, message: 'Please input github/linkedin' }],
                    })(
                        <Input.TextArea
                            prefix={<TransparentIvon type="form" />}
                            placeholder="Introduce yourself"
                            style={{ height: '150.8px' }}
                        />,
                    )}
                </FormItem>
                <FormItem textAlign='text-center'>
                    {getFieldDecorator('dragger', {
                        valuePropName: 'fileList',
                        getValueFromEvent: normFile,
                        rules: [
                            { required: true, message: 'Please input your CV' },
                        ]
                    })(
                        <Upload.Dragger name="files"
                            beforeUpload={() => false}>
                            <p className="ant-upload-drag-icon">
                                <TransparentIvon type="inbox" color="rgba(0,0,0,1)" />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">Support for a single or bulk upload.</p>
                        </Upload.Dragger>
                    )}
                </FormItem>
            </TextAreaAndUploadDiv>
            <ButtonDiv>
                <Form.Item >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </ButtonDiv>
        </FormStyled >
    )
}

const WrappedApplyForm = Form.create({ name: 'apply_form' })(ApplyForm);

export default WrappedApplyForm;