import React from 'react'
import { Redirect, Route } from 'react-router-dom'

import { openNotification } from '../../utils/NotificationUtils'
import { LOGIN_REQUIRED } from '../../constants/Modal/SocialMedia/SOCIALMEDIA_MODAL'

const PrivateRoute = ({ component: Component, loggedIn, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props => {
                if (loggedIn === true) {
                    return <Component {...props} />
                } else {
                    openNotification('warning', 'Access denied', LOGIN_REQUIRED)
                    return <Redirect to={{ pathname: '/' }} />
                }
            }}
        />
    )
}

export default PrivateRoute
