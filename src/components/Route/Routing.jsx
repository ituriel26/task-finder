import React from 'react'
import { connect } from 'react-redux'
import { BrowserRouter, Route } from "react-router-dom";

import PrivateRoute from '../Route/PrivateRoute'
import Dashboard from '../../containers/Dashboard/Dashboard'
import ChatContainer from '../../containers/ChatContainer/ChatContainer'

const Routing = ({ loggedIn }) => {
    return (
        <BrowserRouter>
            <Route component={Dashboard} path="/" exact />
            <PrivateRoute loggedIn={loggedIn} component={ChatContainer} path="/chat" />
        </BrowserRouter>
    )
}

const stateToProps = state => {
    return {
        loggedIn: state.userReducers.loggedIn
    }
}

export default connect(stateToProps, null)(Routing)
