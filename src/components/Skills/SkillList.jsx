import React from 'react';
import styled from 'styled-components';
import { Radio } from "antd";

import { SKILL_LIST } from "../../constants/Skills/SKILL_LIST";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

const SkillRadio = styled(RadioButton)`
    && {
    color: ${props => props.selected === true ? '#fff' : null};
    background: ${props => props.selected === true ? '#1890ff' : null};
    border-color: ${props => props.selected === true ? '#1890ff' : null};
    }
    &&:hover {
        color: ${props => props.selected === true ? '#fff' : null};
    }
`;

const RadioDiv = styled.div`
    flex: 1; 
    display: flex; 
    justify-content: row;
`;

const SkillList = ({ setOptionsFilter, optionsFilter }) => {
    const { skills } = optionsFilter;
    return (
        <RadioDiv>
            <RadioGroup
                value={skills}
                onChange={e => onChange(e, optionsFilter, setOptionsFilter)}
                buttonStyle="solid">
                {SKILL_LIST.map(skill => {
                    return (
                        <SkillRadio
                            selected={skills.includes(skill)}
                            value={skill}
                            key={skill}>
                            {skill}
                        </SkillRadio>
                    )
                })}
            </RadioGroup>
        </RadioDiv>
    )
};

const onChange = ({ target }, optionsFilter, setOptionsFilter) => {
    const { value } = target;
    const { skills } = optionsFilter;

    const newSkills = skills.includes(value) ?
        skills.filter(skill => skill !== value) :
        [...skills, value]

    const newOptionFilter = Object.assign({}, optionsFilter, { skills: newSkills })
    setOptionsFilter(newOptionFilter)
}

export default SkillList