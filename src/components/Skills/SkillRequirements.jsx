import React from 'react'
import styled from 'styled-components';
import { Select } from 'antd';

import {
    EXP_VALUES,
    SALARY_VALUES,
    SELECT_EXP_TITLE,
    SELECT_SALARY_TITLE
} from "../../constants/Skills/SKILLS_REQUIREMENTS";

const { Option } = Select;

const SelectOption = styled(Select)`
    width: 40%; 
    padding-right: 16px;
`;

const SkillRequirementsDiv = styled.div`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`;

export const SkillRequirements = ({ setOptionsFilter, optionsFilter }) => {

    const onChangeSalary = (value) => {
        const copy = Object.assign({}, optionsFilter, { salary: value })
        setOptionsFilter(copy)
    }

    const onChangeExp = (value) => {
        const copy = Object.assign({}, optionsFilter, { exp: value })
        setOptionsFilter(copy)
    }

    return (
        <SkillRequirementsDiv>
            {createSelect(SELECT_SALARY_TITLE, SALARY_VALUES, onChangeSalary)}
            {createSelect(SELECT_EXP_TITLE, EXP_VALUES, onChangeExp)}
        </SkillRequirementsDiv>
    )
};

const createSelect = (TITLE, VALUES, onChange) => {
    return (
        <SelectOption
            showSearch
            placeholder={TITLE}
            onChange={onChange}
            defaultValue={0}
        >
            {VALUES.map((item, index) => {
                return (
                    <Option value={index} key={item}>
                        {item}
                    </Option>
                )
            })}
        </SelectOption>
    );
};

export default SkillRequirements;