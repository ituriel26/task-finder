import React from 'react';
import {shallow} from 'enzyme'
import {Select} from 'antd';

import {SkillRequirements, createSelect} from "../SkillRequirements";
import {
    EXP_VALUES,
    SALARY_VALUES,
    SELECT_EXP_TITLE,
    SELECT_SALARY_TITLE
} from "../../../constants/Skills/SKILLS_REQUIREMENTS";

const {Option} = Select;

describe('SkillRequirements component test', () => {
    let wrapper;
    it('Check if all subcomponent rendered', () => {
        wrapper = shallow(<SkillRequirements/>);
        expect(wrapper.find(Select).length).toBe(2);
        expect(wrapper.find(Option).length).toBe(SALARY_VALUES.length + EXP_VALUES.length);
    });
    describe('SkillRequirements unit tests', () => {
        const result1 = createSelect(SELECT_EXP_TITLE,EXP_VALUES,()=>{});
        const result2 = createSelect(SELECT_SALARY_TITLE,SALARY_VALUES,()=>{});

        it('createSelect, correct input for exp', () => {
            expect(result2.props.children.length).toBe(EXP_VALUES.length);
            expect(result2.props.defaultValue).toBe(0);
        });
        it('createSelect, correct input for salary', () => {
            expect(result1.props.children.length).toBe(SALARY_VALUES.length);
            expect(result1.props.defaultValue).toBe(0)
        });
    });
});
