import React from 'react';
import {shallow} from 'enzyme'

import Skills from "../Skills";

describe('Places component test', () => {
    let wrapper;
    it('Check if all subcomponent rendered', () => {
        wrapper = shallow(<Skills/>);
        expect(wrapper.find('Connect(SkillList)').length).toBe(1);
        expect(wrapper.find('Connect(SkillRequirements)').length).toBe(1);
    });
});