import React from 'react';
import {shallow} from 'enzyme'
import configureStore from 'redux-mock-store'

import SkillListConnected, {SkillList, showSkills} from "../SkillList";
import {Radio} from "antd";
import {SKILL_LIST} from "../../../constants/Skills/SKILL_LIST";

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

const initialState = {
    skillReducers: {
        selectedSkills: [],
    }
};

const middlewares = [];
const mockStore = configureStore(middlewares);

const SELECTED_SKILLS1 = [];
const SELECTED_SKILLS2 = ['JS','PHP'];
const SELECTED_SKILLS3 = ['Tarcie chrzanu'];

describe('SkillList component test', () => {

    describe('SkillList render tests', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = shallow(<SkillList selectedSkills={[]}/>);
        });

        it('Check if all subcomponent rendered', () => {
            expect(wrapper.find(RadioGroup).length).toBe(1);
            expect(wrapper.find(RadioButton).length).toBe(SKILL_LIST.length);
        });

    });
    describe('SkillList unit tests', () => {
        const result1 = showSkills(SELECTED_SKILLS1);
        const result2 = showSkills(SELECTED_SKILLS2);
        const result3 = showSkills(SELECTED_SKILLS3);

        it('showDefaultPlaces, 0 selected radioButtons', () => {
            expect(result1.filter(skill=> skill.props.className === 'ant-radio-button-wrapper-checked').length).toBe(0);
        });
        it('showDefaultPlaces 2 selected radioButtons', () => {
            expect(result2.filter(skill=> skill.props.className === 'ant-radio-button-wrapper-checked').length).toBe(2);
        });
        it('showDefaultPlaces wrong input', () => {
            expect(result3.filter(skill=> skill.props.className === 'ant-radio-button-wrapper-checked').length).toBe(0);
        });
    });
    describe('SkillListConnected tests', () => {
        let wrapper;
        let store;

        beforeEach(() => {
            store = mockStore(initialState);
            wrapper = shallow(<SkillListConnected store={store}/>);
        });

        it('Check SkillList Connected render ', () => {
            expect(wrapper.length).toEqual(1)
        });
        it('Check SkillList Connected props ', () => {
            const props = wrapper.prop('children').props;
            expect(props['selectedSkills']).toEqual(initialState.skillReducers.selectedSkills);
        });
    });

});
