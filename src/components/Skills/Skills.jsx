import React from 'react'
import styled from 'styled-components';

import SkillList from "./SkillList";
import SkillRequirements from "./SkillRequirements";

const SkillDiv = styled.div`
    display: flex;
    margin-top: 16px;
    padding-left: 16px;
    padding-right: 16px;
    align-items: center;
    min-width: 100%;
`;

const Skills = ({ setOptionsFilter, optionsFilter }) => {
    const propsToSkills = {
        setOptionsFilter,
        optionsFilter
    }

    return (
        <SkillDiv>
            <SkillList {...propsToSkills}/>
            <SkillRequirements  {...propsToSkills} />
        </SkillDiv>
    )
};

export default Skills;

