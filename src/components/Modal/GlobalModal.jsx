import React from 'react'
import { connect } from "react-redux";
import { Modal } from 'antd';

import { hideModal } from '../../redux/actions/modalActions'

const GlobalModal = ({ hideModal, visible, content }) => {
    return (
        <Modal title={content.title} visible={visible} footer={null} onCancel={hideModal}>
            {content.content}
        </Modal>
    )
}

const stateToProps = state => {
    return {
        visible: state.modalReducers.visible,
        content: state.modalReducers.content,
    };
};

const actionToProps = dispatch => {
    return {
        hideModal: () => dispatch(hideModal())
    }
};

export default connect(stateToProps, actionToProps)(GlobalModal)

