import React from 'react'
import styled from 'styled-components'
import { connect } from "react-redux";
import { Icon, Row, Col } from 'antd'

import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'

import { NOTIFICATION_ERROR, MESSAGE_GOOGLE, DESCRIPTION_GOOGLE } from '../../../constants/Modal/SocialMedia/SOCIALMEDIA_MODAL'
import { REDIRECT_URL, ICON_SIZE, facebook_clientId, google_clientId } from '../../../constants/Modal/SocialMedia/SOCIALMEDIA_MODAL'
import { openNotification } from '../../../utils/NotificationUtils'
import { getAccessTokenGoogle } from '../../../services/SocialMediaAuthService'
import { saveToken, saveUser, logIn } from '../../../redux/actions/userActions'

const SocialIcon = styled(Icon)`
    font-size: ${props => props.size};
`;

const SocialMediaNodal = ({ logIn, saveToken, saveUser, hideModal }) => {
    return (
        <Row justify="center" align="top">
            <Col span={12} style={{ textAlign: 'center' }}>
                <FacebookLogin
                    appId={facebook_clientId}
                    callback={e => console.log(e)}
                    render={renderProps => <SocialIcon type="facebook" size={ICON_SIZE} onClick={renderProps.onClick} />}
                />
            </Col>
            <Col span={12} style={{ textAlign: 'center' }}>
                <GoogleLogin
                    clientId={google_clientId}
                    render={renderProps => <SocialIcon type="google" size={ICON_SIZE} onClick={renderProps.onClick} disabled={renderProps.disabled} />}
                    onSuccess={e => getAccessTokenGoogle(e)
                        .then((res) => {
                            saveUser(e.profileObj)
                            saveToken(res)
                            logIn()
                        })
                        .then(hideModal)
                        .catch(() => openNotification(NOTIFICATION_ERROR, MESSAGE_GOOGLE, DESCRIPTION_GOOGLE))}
                    onFailure={() => openNotification(NOTIFICATION_ERROR, MESSAGE_GOOGLE, DESCRIPTION_GOOGLE)}
                    cookiePolicy={'single_host_origin'}
                    redirectUri={REDIRECT_URL} />
            </Col>
        </Row>
    )
}

const actionToProps = dispatch => {
    return {
        saveToken: (content) => dispatch(saveToken(content)),
        saveUser: (content) => dispatch(saveUser(content)),
        logIn: () => dispatch(logIn()),
    }
};

export default connect(null, actionToProps)(SocialMediaNodal)
