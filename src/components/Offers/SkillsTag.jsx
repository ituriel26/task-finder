import React from 'react'
import { Tag } from "antd";
import uuid from 'uuid'
import styled from 'styled-components'

const STag = styled(Tag)`
&& {
    padding: 4px;  
}`;

export const SkillsTag = ({ skills }) => {
    return (
        skills.map(skill => {
            return (
                <STag key={uuid()}>
                    {skill}
                </STag>
            )
        })
    )
};

