import React from 'react'
import styled from 'styled-components'

import { LanguageIcon } from "./LanguageIcon";
import { ExpTag } from "./ExpTag";
import { SalaryTag } from "./SalaryTag";
import { DataTag } from "./DataTag";
import { CompanyDetails } from "./CompanyDetails";
import { SkillsTag } from "./SkillsTag";

const A = styled.a`
   && {
    flex: 1;
    display: flex;
    border-radius: 5px;
    border: solid;
    border-width: thin;
    margin: 10px;
    padding-bottom: 10px;
    &:hover {
        background-color: #f0faff
    }
   }
`;

const FlexDiv = styled.div`
    flex:1;
    display: flex;
`;

const OfferDiv = styled(FlexDiv)`
    flex-direction: column;
`;

const FirstRow = styled(FlexDiv)`
    align-items: center;
`;

const TitleDiv = styled(FlexDiv)`
    align-items: center;
    justify-content: flex-start;
    font-size: 20px;
`;

const OfferDetailsTagDiv = styled(FlexDiv)`
    align-items: center;
    justify-content: flex-end;
`;

const SecondRow = FlexDiv;

const CompanyDiv = styled(FlexDiv)`
    align-items: center;
`;

const OtherSkillsTagsDiv = styled(FlexDiv)`
    align-items: center;
    justify-content: flex-end;
`;

const SingleOffer = ({ offer, setHoveredOffer, fetchDetails }) => {
    const {
        primarySkill,
        title,
        exp,
        salaryMin,
        salaryMax,
        createdTime,
        companyName,
        localization,
        secondarySkills
    } = offer;

    return (
        <A
            onClick={fetchDetails}
            onMouseEnter={() => setHoveredOffer(offer)}
            onMouseLeave={() => setHoveredOffer(null)}>
            <LanguageIcon
                language={primarySkill} />
            <OfferDiv>
                <FirstRow>
                    <TitleDiv>
                        {title}
                    </TitleDiv>
                    <OfferDetailsTagDiv>
                        <ExpTag exp={exp} />
                        <SalaryTag
                            salaryMin={salaryMin}
                            salaryMax={salaryMax} />
                        <DataTag createdAt={createdTime} />
                    </OfferDetailsTagDiv>
                </FirstRow>
                <SecondRow>
                    <CompanyDiv>
                        <CompanyDetails
                            companyName={companyName}
                            localization={localization} />
                    </CompanyDiv>
                    <OtherSkillsTagsDiv>
                        <SkillsTag skills={secondarySkills} />
                    </OtherSkillsTagsDiv>
                </SecondRow>
            </OfferDiv>
        </A>
    )
};


export default SingleOffer