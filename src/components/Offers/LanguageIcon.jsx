import React from 'react'
import styled from 'styled-components'

const Img = styled.img`
    height: 50px; 
    width: 50px; 
    margin: 16px;
`;

export const LanguageIcon = ({ language }) => {
    const src = pathToIcon(language);
    return (
        <Img src={src} />
    )
};

const pathToIcon = language => {
    return `${process.env.PUBLIC_URL}/icons/${language}.png`
}