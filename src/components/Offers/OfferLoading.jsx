import React from 'react'
import styled from 'styled-components'
import {Spin} from "antd";

const SpinDiv = styled.div`
    flex: 1;
    height: 100px;
    display: flex;
    border-radius: 5px;
    border: solid;
    border-width: thin;
    margin: 10px;
    padding-bottom: 10px;
    padding-top: 10px;
    align-items: center;
    justify-content: center;
`;

const OfferLoading = () => {
    return (
        <a>
            <SpinDiv>
                <Spin size="large"/>
            </SpinDiv>
            <SpinDiv>
                <Spin size="large"/>
            </SpinDiv>
        </a>
    )
};

export default OfferLoading