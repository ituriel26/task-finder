import React from 'react'
import styled from 'styled-components'
import {TAG_COLOR} from "../../constants/Offers/TAG_COLORS";
import Tag from "antd/lib/tag";

const INTERVAL = 2300;

const STag = styled(Tag)` 
&& {
    margin: 9px;
    padding: 4px;  
}`;

export const SalaryTag = ({salaryMin, salaryMax}) => {
    const color = selectColor(salaryMin);

    return (
        <STag color={color}>
            {`${salaryMin} - ${salaryMax}`}
        </STag>
    )
};

const selectColor = salaryMin => {
    const option = Math.round(salaryMin / INTERVAL) > 11 ? 11 : Math.round(salaryMin / INTERVAL);
    return TAG_COLOR[option];
};