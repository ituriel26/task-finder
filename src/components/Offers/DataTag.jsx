import React from 'react'
import {Tag} from 'antd';
import styled from 'styled-components'

const DTag = styled(Tag)`
&& {
    margin: 9px;
    padding: 4px;  
}`;

const SECONDS_IN_DAY = 86400;
const SECUNDE_IN_MILISECUNDE = 1000;

export const DataTag = ({createdAt}) => {
    const dataDiff = Math.floor((Math.round(Date.now() / SECUNDE_IN_MILISECUNDE) - createdAt) / SECONDS_IN_DAY);
    const display = (dataDiff === 0) || (dataDiff === 1) ? `New` : `${dataDiff} days ago`;
    const color = dataDiff === 0 ? 'red' : 'geekblue';

    return (
        <DTag color ={color}>
            {display}
        </DTag>
    )
};