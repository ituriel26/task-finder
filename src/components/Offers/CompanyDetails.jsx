import React from 'react'
import styled from 'styled-components'
import {Icon} from "antd";


const CompanyDiv = styled.div`
    display: flex;
    flex:1;
`;

const CompanyData = styled.div`
    padding-right: 25px;
`;

export const CompanyDetails = ({companyName, localization}) => {
    return (
        <CompanyDiv>
            <CompanyData>
                <Icon type="home"/>
                {companyName}
            </CompanyData>
            <CompanyData>
                <Icon type="tags"/>
                {localization}
            </CompanyData>
        </CompanyDiv>
    )
};

