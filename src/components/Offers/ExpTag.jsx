import React from 'react'
import styled from 'styled-components'
import { Tag } from 'antd';
import { EXP_COLOR_MAP } from '../../constants/Offers/TAG_COLORS';


const ETag = styled(Tag)`
&& {
    margin: 9px;
    padding: 4px;  
}`;

export const ExpTag = props => {
    const { exp } = props;

    return (
        <ETag color={EXP_COLOR_MAP.get(exp)}>
            {exp}
        </ETag>
    )
};

