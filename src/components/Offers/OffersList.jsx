import React from 'react'
import styled from 'styled-components'

import uuid from "uuid";
import InfiniteScroll from 'react-infinite-scroller';

import { EXP_VALUES } from "../../constants/Skills/SKILLS_REQUIREMENTS";
import { DEFAULT_CITIES } from '../../constants/Places/CITIES'
import SingleOffer from "./SingleOffer";
import OfferLoading from "./OfferLoading";

const InfiniteScrollDiv = styled.div` 
    flex: 1;
    width: 100%;
    height: 80vh;
    overflow: auto;
`;

const OffersList = (props) => {
    const {
        offers,
        fetchMoreOffers,
        hasMore,
        fetchDetails,
        optionsFilter,
        setHoveredOffer
    } = props;

    const filterCityAndSkillCallback = createCityAndSkillFilterCallback(optionsFilter);
    const filterSalaryAndExpCallback = createSalaryAndExpFilterCallback(optionsFilter);

    return (
        <InfiniteScrollDiv>
            <InfiniteScroll
                useWindow={false}
                pageStart={0}
                loadMore={() => fetchMoreOffers()}
                hasMore={hasMore}
                loader={<OfferLoading key={uuid()} />}>
                {offers && offers
                    .filter(filterCityAndSkillCallback)
                    .filter(filterSalaryAndExpCallback)
                    .map(offer => createSingleOffer(offer, setHoveredOffer, fetchDetails))}
            </InfiniteScroll>
        </InfiniteScrollDiv>
    )
};

const createSingleOffer = (offer, setHoveredOffer, fetchDetails) => {
    const propsToSingleOffer = {
        offer,
        fetchDetails,
        setHoveredOffer
    }
    return <SingleOffer key={offer.id} {...propsToSingleOffer} />
}

const createSalaryAndExpFilterCallback = ({ salary, exp }) => {
    if (salary !== 0 && exp === 0)
        return (offer) =>
            offer.salaryMin < (salary * 5000) && ((salary + 1) * 5000) > offer.salaryMax;
    else if (salary !== 0 && exp !== 0)
        return (offer) =>
            (offer.salaryMin < (salary * 5000) && ((salary + 1) * 5000) > offer.salaryMax) && offer.exp === EXP_VALUES[exp];
    else if (salary === 0 && exp !== 0)
        return (offer) => offer.exp === EXP_VALUES[exp];
    else
        return () => true;
};

const createCityAndSkillFilterCallback = ({ place, skills }) => {
    if (skills.length > 0 && place !== DEFAULT_CITIES[0])
        return (offer) => (skills.includes(offer.primarySkill)
            && offer.localization === place);
    else if (skills.length > 0 && place === DEFAULT_CITIES[0])
        return (offer) => skills.includes(offer.primarySkill);
    else if (skills.length === 0 && place !== DEFAULT_CITIES[0])
        return (offer) => offer.localization === place;
    else
        return () => true;
};

export default OffersList;

