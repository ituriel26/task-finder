import {generateMockDataAsync, generateMockChatHistoryAsync} from '../mock/Chat/MockUserList'

const QUANTITY = 10;

export const fetchUserList = async () => {
    return generateMockDataAsync(QUANTITY);
}

export const fetchChatHistory = async (chatId) => {
    return generateMockChatHistoryAsync(QUANTITY);
}
