import qs from 'qs'
import { openNotification } from '../utils/NotificationUtils'
import { URL, CONNECT_TOKEN } from '../constants/SERVER_CONSTANTS'
import {NOTIFICATION_ERROR, MESSAGE_GOOGLE, DESCRIPTION_GOOGLE } from '../constants/Modal/SocialMedia/SOCIALMEDIA_MODAL'

export const getAccessTokenGoogle = async ({ tokenId }) => {
    const data = {
        'token': tokenId,
        'grant_type': 'Google',
    }

    return fetch(`${URL}${CONNECT_TOKEN}`, {
        method: 'POST',
        body: qs.stringify(data),
        mode: 'cors',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
    })
        .then(res => res.json())
        .catch(() => openNotification(NOTIFICATION_ERROR, MESSAGE_GOOGLE, DESCRIPTION_GOOGLE))
}

export const saveTokenFacebook = () => {

}


