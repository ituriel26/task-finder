import {generateMockDataAsync} from '../mock/Offers/MockOffers'
import {generateMockDataDetailsAsync} from '../mock/Offers/SingleOffer'

const QUANTITY = 20;

export const fetchOffers = async () => {
    return generateMockDataAsync(QUANTITY);
}

export const fetchOfferDetails = async () => {
    return generateMockDataDetailsAsync();
}