import { sampleDescription } from './SampleDescription'

export const generateMockDataDetailsAsync = async () => {
    return new Promise(resolve => {
        setTimeout(() => resolve(singleOffer), 1200)
    })
};

const singleOffer = {
    id: "123",
    skills: [{ skillName: 'Obciaganie bez polyku', stars: 5, }, { skillName: 'Sprawność rąk', stars: 5, }, { skillName: 'Ciasność odbytu', stars: 5, }, { skillName: 'Small talk', stars: 1, }, { skillName: 'Ładność twarzy', stars: 2, }, { skillName: 'Higiena', stars: 3, }],
    companyDetails: [{ ahref: 'https://www.facebook.com/Marjan118', title: 'Marjan', details: 'Company name', }, { title: '1', details: 'Company size', }, { title: 'B2B', details: 'EMP, type' }, { title: 'Mid', details: 'EXP, lvl', }, { title: 'New', details: 'Added' },],
    src: 'https://scontent.fktw1-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/61346751_2226997307388810_460593190471204864_n.jpg?_nc_cat=110&_nc_oc=AQkZL5ftTrxHYa6hVPMsX3q9--tDpbueHX5YMgnh0tjbsMDg-CGMvQLRKHpb6BUH2u8&_nc_ht=scontent.fktw1-1.fna&oh=f36d39a7fcdf03a8cb59b13073692dfc&oe=5E207568',
    salary: '10000 - 12000 PLN net/month',
    title: 'Product Designer',
    adress: 'ul. Kolejowa 5/7, Warszawa',
    sampleDescription,
}