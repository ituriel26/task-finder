import uuid from 'uuid'
import { DEFAULT_CITIES } from "../../constants/Places/CITIES";
import { SKILL_LIST } from "../../constants/Skills/SKILL_LIST";
import { EXP_VALUES } from "../../constants/Skills/SKILLS_REQUIREMENTS";

const title = ['Tech Lead', 'Data Control and Support Specialist', ' Konsultant Microsoft Dynamics 365 / CRM CE', 'Developer', 'Fullstack', 'Tester', 'Scrum master', 'PM'];
const companyName = ['PKN Orlen', 'Polskie Górnictwo Naftowe i Gazownictwo', 'Grupa Lotos', 'PGE', 'KGHM Polska Miedź', 'Tauron', 'Cinkciarz.pl', 'PKO BP', 'PSH Lewiatan', 'PPHU Specjał', 'Enea', 'Cyfrowy Polsat', 'PGL Lasy Państwowe', 'Energa', 'Polskie Sieci Elektroenergetyczne', 'Grupa Azoty', 'JSW', 'Pekao'];
const localization = [...DEFAULT_CITIES];

export const generateMockDataAsync = async (quantity) => {
    return new Promise(resolve => {
        setTimeout(() => resolve([generateMockData(quantity), Math.random() > 0.5 ? true : false]), 1200)
    })
};

const generateMockData = (quantity) => {
    return Array.from(new Array(quantity))
        .map(() => {
            const salary = (Math.random() * (25000 - 5000) + 5000);
            return {
                id: uuid(),
                title: title[Math.floor(Math.random() * title.length)],
                companyName: companyName[Math.floor(Math.random() * companyName.length)],
                localization: localization[Math.floor(Math.random() * localization.length)],
                salaryMin: `${Math.round(salary)}`,
                salaryMax: `${Math.round(salary + 2000)}`,
                createdTime: 1570300253,
                lat: 52.229676 + Math.random(),
                lng: 21.012229 + Math.random(),
                exp: EXP_VALUES[Math.floor(Math.random() * (EXP_VALUES.length - 1)) + 1],
                primarySkill: SKILL_LIST[Math.floor(Math.random() * SKILL_LIST.length)],
                secondarySkills: [SKILL_LIST[Math.floor(Math.random() * SKILL_LIST.length)], SKILL_LIST[Math.floor(Math.random() * SKILL_LIST.length)], SKILL_LIST[Math.floor(Math.random() * SKILL_LIST.length)]]
            }
        });
};
