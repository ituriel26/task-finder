import React from 'react'
export const sampleDescription = <div>
    <div>Cześć,<br /><br />Jeśli czujesz, że jesteś już na wyższym poziomie niż <strong>MID JS developer</strong>, chcesz się rozwijać - zobacz dlaczego powinieneś aplikować do Skygate!<br /><br /><strong>Projekty - czym będziesz się zajmować:</strong></div>
    <ul>
        <li>Szukamy perfekcji w dw&oacute;ch uzupełniających się obszarach:
<ul>
                <li>dobrze finansowane startupy (Y Combinator, 500 Startups)</li>
                <li>działy rozwojowe największych firm (Fortune 500).</li>
            </ul>
        </li>
        <li>Od konkurencji odr&oacute;żnia nas fakt, że od pierwszego dnia projektu będziesz mieć bezpośredni kontakt z Klientem na Slacku, bywa że na wideokonferencjach. Spokojnie, dostaniesz od nas wszelkie wsparcie, a bezpośredni kontakt pomoże Ci lepiej zrozumieć problem i projekt, nad kt&oacute;rym pracujesz.</li>
        <li>Klienci szukają u nas jakości, nie pośpiechu - niemniej funkcjonujemy w realiach ram czasowych lub innych ograniczeń. Wynika to podejścia typu Greenfield + Customer Development -&gt; szukanie modelu biznesowego i testowanie go na realnych Klientach. Nastaw się na dużo adrenaliny i dobrej zabawy.</li>
        <li>Tematy aplikacji, kt&oacute;re odpaliliśmy niedawno:
<ul>
                <li>wizualizacja bazy danych - 10M+ rekord&oacute;w real-time w formie grafu, React, SVG, Redux, RxJS</li>
                <li>marketplace łączący trener&oacute;w i osoby poszukujące szkoleń/porad - React, TypeScript, Redux-Observable, CSS-in-JS</li>
                <li>edytor + marketplace podcast&oacute;w - React, Redux-Observable, Node.js/Express, Elasticsearch, Mongo, AWS, Docker, Emotion (css-in-js), server-sent events, MediaRecorder API, HowlerJS</li>
                <li>zarządzanie firmą dostarczającą voip + internet i usługi tv - Angular, ngrx, jest, protractor, karma</li>
                <li>apka do poszukiwania niani - React Native, Expo, GraphQL, Apollo</li>
                <li>statyczne strony internetowe w Gatsby'm</li>
                <li>tajemnicza aplikacja w Vue + wielki i skomplikowany dashboard z raportami - dużo matmy, SVG</li>
            </ul>
        </li>
        <li>Stos Devops - devops is a culture, not a person - nie mamy dedykowanego Devopsa, uczymy się i ogarniamy sami, możesz liczyć na pomoc doświadczonych os&oacute;b:
<ul>
                <li>orkiestrację kontener&oacute;w migrujemy obecnie z Docker Compose na Kubernetes</li>
                <li>CI + CD: Jenkins + Jenkins Pipelines + Jenkins Plugins</li>
                <li>Infrastructure as Code: Hashicorp, czyli Terraform + Vault + Consul + Nomad</li>
                <li>Hosting: AWS, Google Cloud, Bare metal: OVH</li>
            </ul>
        </li>
        <li>Testy powinny być u nas nieodłączną częścią pull request&oacute;w</li>
        <li>Dbamy o dobrą komunikację z Klientem, regularnie odwiedzamy Klient&oacute;w (m.in. w Stanach) - na miejscu lub oni nas w Gliwicach.</li>
    </ul>
    <div><br /><strong>Filozofia:</strong></div>
    <ul>
        <li>Cokolwiek nowego z JSa pojawia się na Twitterze / Medium.com - my już to rozkminiamy, staramy się używać w projektach. Nasi Klienci oczekują od nas znajomości najnowszych trend&oacute;w.</li>
        <li>Dobra atmosfera w biurze i po pracy: kajaki, trampoliny, paintball, krafty, wspinaczka, futsal - zajrzyj na naszego <a href="https://www.instagram.com/skygate.io/" target="_blank">Instagrama</a> + zasugeruj sw&oacute;j pomysł, a pewnie tam pojedziemy!</li>
        <li>Praktyki, Staże, warsztaty, prezentacje, kultura uczenia jest wpisana w nasze DNA - szkoląc innych, usystematyzujesz swoją własną wiedzę.</li>
        <li>FullStack mindset - jeśli tylko chcesz - nauczymy Cię Node'a, Pythona, Data Science, Machine Learningu, dApps - będzie się od kogo uczyć!</li>
        <li>Głęboka woda + wsparcie kiedy go potrzebujesz, nie będziesz się nudzić - chcemy zagwarantować dużo rozwoju!</li>
        <li>Weź udział i dziel się wiedzą w wydarzeniach kt&oacute;re organizujemy lub wspieramy:
<ul>
                <li>Django Girls,</li>
                <li>skylabs,</li>
                <li>MeetJS,</li>
                <li>CodersDay&nbsp;</li>
                <li>skyhacks</li>
                <li>syktalks</li>
            </ul>
        </li>
        <li>Rośniemy, przez ostatni rok podwoiliśmy załogę i nie zatrzymujemy się :)</li>
        <li>R&oacute;wnie ważne jak twarde są dla nas miękkie umiejętności. Dobre radzenie sobie w stresie, praca zespołowa, motywowanie innych, techniczne reprezentowanie nas przed Klientami w projekcie, sugerowanie rozwiązań, otwarta głowa - tego wszystkiego będziesz miał szansę zakosztować i z naszym wsparciem - rozwinąć r&oacute;wnież te, bardzo w dzisiejszych czasa ważne umiejętności</li>
        <li>Wzajemne code review - dla wyższej jakości oraz aby wiedzieć, co dzieje się w pozostałych projektach.</li>
    </ul>
    <div><strong>Czego wymagamy:</strong></div>
    <ul>
        <li>dw&oacute;ch lat doświadczenia komercyjnego jako JS developer lub przekonania nas, że jesteś na takim poziomie</li>
        <li>znajomości Reacta, React Native, Vue lub Angulara przynajmniej na podstawowym poziomie + otwartość na inne frameworki</li>
        <li>wiesz z czym się je Mocha, Sinon, Jest lub Enzyme</li>
        <li>doświadczenie z TypeScriptem</li>
        <li>brak problem&oacute;w z matematyką, abstrakcyjnym myśleniem</li>
        <li>bardzo dobrej znajomości ESNext</li>
        <li>Rozumienie na czym polega asynchroniczność i jak sobie z nią radzić w JS (Promise, Observable)</li>
        <li>dobrej znajomości wzorc&oacute;w algorytm&oacute;w i architektury oprogramowania (m.in. dependency injenction, MV*, Flux) (m.in. dependency injenction)</li>
        <li>gry zespołowej oraz serca do piłkarzyk&oacute;w (lub innych sport&oacute;w)</li>
        <li>chęci do wsp&oacute;łtworzenia firmy programistycznej i pięcia się w g&oacute;rę</li>
        <li>płynnej znajomości języka polskiego</li>
        <li>płynnej znajomości języka angielskiego</li>
        <li>umiejętność analitycznego myślenia</li>
        <li>umiejętność samodzielnego rozwiązywania problem&oacute;w</li>
        <li>pokład&oacute;w energii do samodzielnego doskonalenia swoich umiejętności</li>
    </ul>
    <div><br /><strong>W trakcie rekrutacji spodziewaj się:</strong></div>
    <ul>
        <li>zaangażowania z naszej strony, do rekrutacji staramy się podchodzić poważnie</li>
        <li>możliwie szybkiej komunikacji</li>
        <li>otrzymasz od nas zadania testowe, po kt&oacute;rym otrzymasz feedback od doświadczonego programisty + wskaz&oacute;wki co można było zrobić lepiej</li>
        <li>w przypadku dużej ilości kandydat&oacute;w damy Ci znać ile czasu potrzebujemy na przeanalizowanie Twojej kandydatury</li>
        <li>będziemy chcieli r&oacute;wnież poznać Twoją opinię o nas, o procesie rekrutacji, chcemy usłyszeć co możemy usprawnić</li>
    </ul>
    <div><br /><strong>Finanse i Perksy:</strong></div>
    <ul>
        <li>- wynagrodzenie - dajemy Ci pełen wyb&oacute;r co do umowy zgodnie z kt&oacute;rą chcesz pracować:
<ul>
                <li>7.5 - 12k net b2b (+ ew. VAT)</li>
                <li>lub 6.5k - 9.6k netto uod (+ NFZ)</li>
                <li>lub 5.8k - 8.5k brutto uop</li>
                <li>lub 5.8k - 9k netto uoz (studenci + NFZ)</li>
                <li>Więcej wyliczeń - <a href="http://kalkulatory.nf.pl/kalkulator/wynagrodzenie/porownaj" target="_blank">Kalkulator Wynagrodzeń.</a></li>
            </ul>
        </li>
        <li>elastyczne godziny pracy, podejście zadaniowe</li>
        <li>darmowe lekcje angielskiego (native speaker)</li>
        <li>dobrze zaopatrzona kuchnia w tym świetna kawa z ekspresu</li>
        <li>możliwość częściowej pracy zdalnej (do 2 dni w tygodniu)</li>
        <li>prywatna opieka zdrowotna, darmowa siłownia, spotify premium, dopłata do multisport</li>
        <li>szansa na delegację za granicę (na radarze Los Angeles, Floryda, Barcelona, Kolonia, Edynburg, Bazylea)</li>
        <li>ciągle udoskonalamy, powiększamy i doposażamy biuro w samym Rynku Gliwic + bezpłatny parking dla Team&rsquo;u</li>
        <li>pakiet relokacyjny - nie musisz być z Gliwic czy ze Śląska, pomożemy Ci się komfortowo przenieść</li>
    </ul>
    <div>Prosimy o załączanie CV w języku angielskim!</div>
</div>