import uuid from 'uuid'

export const generateMockDataAsync = async (quantity) => {
    return new Promise(resolve => {
        setTimeout(() => resolve([generateMockData(quantity), Math.random() > 0.5 ? true : false]), 1000)
    })
};

export const generateMockChatHistoryAsync = async (quantity) => {
    return new Promise(resolve => {
        setTimeout(() => resolve([generateMockChatHistory(quantity), Math.random() > 0.5 ? true : false]), 1000)
    })
};

const generateMockData = (quantity) => {
    return Array.from(new Array(quantity))
        .map(() => {
            return {
                id: uuid(),
                avatarUrl: 'https://scontent.fktw1-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/61346751_2226997307388810_460593190471204864_n.jpg?_nc_cat=110&_nc_oc=AQkZL5ftTrxHYa6hVPMsX3q9--tDpbueHX5YMgnh0tjbsMDg-CGMvQLRKHpb6BUH2u8&_nc_ht=scontent.fktw1-1.fna&oh=f36d39a7fcdf03a8cb59b13073692dfc&oe=5E207568',
                username: 'Marjan Kaleta' + Math.random(),
                lastMessage: 'CHUJU',
                date: '16:22',
                chatId: Math.random()
            }
        });
};

const generateMockChatHistory = (quantity) => {
    return Array.from(new Array(quantity))
        .map(() => {
            if (Math.random() > 0.5) {
                return {
                    id: uuid(),
                    avatarUrl: 'https://scontent.fktw1-1.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/61346751_2226997307388810_460593190471204864_n.jpg?_nc_cat=110&_nc_oc=AQkZL5ftTrxHYa6hVPMsX3q9--tDpbueHX5YMgnh0tjbsMDg-CGMvQLRKHpb6BUH2u8&_nc_ht=scontent.fktw1-1.fna&oh=f36d39a7fcdf03a8cb59b13073692dfc&oe=5E207568',
                    username: 'Marjan Kaleta',
                    message: 'CHUJU WEZ SIE ZA ROBOTE' + Math.random(),
                    date: '16:22',
                }
            } else {
                return {
                    id: uuid(),
                    avatarUrl: 'https://scontent.fwaw3-2.fna.fbcdn.net/v/t31.0-1/c71.0.240.240a/p240x240/10506738_10150004552801856_220367501106153455_o.jpg?_nc_cat=1&_nc_oc=AQnKunQKi0Tcvn-DUIZzExAt131N5Xh0ggmZpPL_zkOH2Csfa2gfhTP2-Vb-ulj8d88&_nc_ht=scontent.fwaw3-2.fna&oh=90001415a8cecec34d86ec26da479b6c&oe=5E584AF3',
                    username: 'Bartosz Gołębiowski',
                    message: 'Daj mi spokój' + Math.random(),
                    date: '16:22',
                }
            }
        });
};