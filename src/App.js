import React from 'react';
import { Provider } from 'react-redux'
import store from './redux/store'

import Modal from './components/Modal/GlobalModal';
import Routing from './components/Route/Routing'

const App = () => {
    return (
        <Provider store={store}>
            <Modal />
            <Routing />
        </Provider>
    );
};

export default App;
