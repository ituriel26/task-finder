import React, { useState } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'

import Chat from '../../components/Chat/Chat/Chat.jsx'
import UserList from '../../components/Chat/UserList/UserList.jsx'
import Header from '../../components/Header/Header.jsx'
import ChatRadio from '../../components/Chat/UserList/ChatRadio'

import { fetchUserList, fetchChatHistory } from '../../services/ChatService'

import useFetchPadding from '../../hooks/useFetch/useFetchPadding'
import useFetchPaddingChat from '../../hooks/useFetch/useFetchPaddingChat'

const ChatDiv = styled.div`
    display: flex;
    flex : 1;
    flex-direction: ${props => props.flexDirection ? props.flexDirection : 'column'};
`;

const ChatContainer = ({user}) => {
    const [radio, setRadio] = useState(0);
    const [selectedChat, setSelectedChat] = useState(0);

    const [bossUserList, fetchMoreBoss, hasMoreBoss] = useFetchPadding(fetchUserList)
    const [customerUserList, fetchMoreCustomer, hasMoreCustomer] = useFetchPadding(fetchUserList)
    const [normalUserList, fetchMoreNormal, hasMoreNormal] = useFetchPadding(fetchUserList)
    const [chatHistory, fetchMoreChatHistory, addMessage] = useFetchPaddingChat(fetchChatHistory)

    const [userListProps] = whichUserFromChatListToDisplay(
        radio,
        setRadio,
        setSelectedChat,
        selectedChat,
        [bossUserList, customerUserList, normalUserList],
        [fetchMoreBoss, fetchMoreCustomer, fetchMoreNormal],
        [hasMoreBoss, hasMoreCustomer, hasMoreNormal]
    )

    const [chatProps] = extractNameAndUrlFromUserList(
        radio,
        user.name,
        [bossUserList, customerUserList, normalUserList],
        selectedChat,
        chatHistory,
        fetchMoreChatHistory,
        addMessage
    )

    return (
        <ChatDiv>
            <Header />
            <ChatDiv flexDirection={'row'}>
                <UserList {...userListProps} />
                <Chat {...chatProps} />
            </ChatDiv>
        </ChatDiv>
    )
}

const whichUserFromChatListToDisplay = (radio, setRadio, setSelectedChat, selectedChat, userLists, userFetch, hasAnyMore) => {
    return [{
        userList: userLists[radio],
        fetchMoreUserList: userFetch[radio],
        hasMore: hasAnyMore[radio],
        chatRadio: <ChatRadio setRadio={setRadio} setSelectedChat={setSelectedChat} />,
        setSelectedChat,
        selectedChat
    }]
}

const extractNameAndUrlFromUserList = (radio, selfName, userLists, chatId, chatHistory, fetchMoreChatHistory, addMessage) => {
    const headerData = userLists[radio]
        .filter(o1 => o1.chatId === chatId)
        .map(o1 => {
            return {
                url: o1.avatarUrl,
                name: o1.username
            }
        })[0];
    return [{
        headerData,
        selfName,
        chatId,
        chatHistoryMap: chatHistory.get(chatId),
        fetchMoreChatHistory,
        addMessage
    }]
}
const stateToProps = state => {
    return {
        user: state.userReducers.user,
    };
};

export default connect(stateToProps, null)(ChatContainer);

