import React, { useState } from 'react'
import styled from 'styled-components'

import Header from "../../components/Header/Header";
import Places from "../../components/Places/Places";
import Skills from "../../components/Skills/Skills";
import OffersList from "../../components/Offers/OffersList";
import OfferMap from '../../components/Map/Map';
import OfferToDisplay from '../../components/OfferToDisplay/OfferToDisplay'

import { fetchOffers, fetchOfferDetails } from '../../services/OfferService'
import useFetchPadding from '../../hooks/useFetch/useFetchPadding'
import useFetch from '../../hooks/useFetch/useFetch'

const DashboardDiv = styled.div`
    display: flex;
    flex-direction: column;
`;

const OffersAndMapDiv = styled.div`
    display: flex;
    flex: 1;
    min-width: 100%;
    height: 80vh;
    padding-top: 25px;
    padding-left: 16px;
`;

export const Dashboard = () => {
    const [hoveredOffer, setHoveredOffer] = useState({})
    const [optionsFilter, setOptionsFilter] = useState({ skills: [], exp: 0, salary: 0, place: 'All' })
    const [offers, fetchMoreOffers, hasMore] = useFetchPadding(fetchOffers)
    const [offerToDisplay, fetchDetails, clearOfferDetails] = useFetch(fetchOfferDetails);

    const propsToPlaces = {
        setOptionsFilter,
        optionsFilter
    }

    const propsToSkills = {
        setOptionsFilter,
        optionsFilter
    }

    const propsToOfferList = {
        offers,
        fetchMoreOffers,
        hasMore,
        optionsFilter,
        fetchDetails,
        setHoveredOffer
    }

    const propsToOfferToDisplay = {
        offerToDisplay,
        clearOfferDetails
    }

    const propsToMap = {
        offerToDisplay,
        hoveredOffer
    }

    const offerComponent = isSelectedOffer(propsToOfferToDisplay, propsToOfferList);

    return (
        <DashboardDiv>
            <Header />
            <Places {...propsToPlaces} />
            <Skills {...propsToSkills} />
            <OffersAndMapDiv>
                {offerComponent}
                <OfferMap {...propsToMap} />
            </OffersAndMapDiv>
        </DashboardDiv>
    )
}

const isSelectedOffer = (propsToOfferToDisplay, propsToOfferList) => {
    const { offerToDisplay } = propsToOfferToDisplay;
    return !offerToDisplay || Object.keys(offerToDisplay).length === 0 ?
        <OffersList {...propsToOfferList} /> :
        <OfferToDisplay {...propsToOfferToDisplay} />
}

export default Dashboard;


