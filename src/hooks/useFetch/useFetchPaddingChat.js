import { useState } from 'react'

const useFetchPaddingChat = (fetchFunction) => {
    const [data, setData] = useState(new Map());
    const fetchRequest = async (chatId) => {
        fetchFunction(chatId)
            .then(res => {
                if (data.has(chatId)) {
                    const copyChat = new Map(data)
                    copyChat.set(chatId, { 'history': [...res[0], ...copyChat.get(chatId)['history']], 'hasMore': res[1] })
                    setData(copyChat)
                } else {
                    const copyChat = new Map(data)
                    copyChat.set(chatId, { 'history': [...res[0]], 'hasMore': res[1] })
                    setData(copyChat)
                }
            });
    }
    const addMessage = (chatId, message) => {
        const copyChat = new Map(data)
        copyChat.set(chatId, { 'history': [...copyChat.get(chatId)['history'], message], 'hasMore': copyChat.get(chatId)['hasMore'] })
        setData(copyChat)
    }

    return [data, fetchRequest, addMessage]
}

export default useFetchPaddingChat