import { useState } from 'react';

const useFetch = (fetchFunction) => {
    const [data, setData] = useState([]);
    const reset = () => setData({})
    const fetch = async () => {
        fetchFunction()
            .then(res => setData(res));
    }

    return [data, fetch, reset]
}

export default useFetch
