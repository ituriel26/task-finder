import { useState } from 'react';

const useFetchPadding = (fetchFunction) => {
    const [data, setData] = useState([]);
    const [hasMore, setHasMore] = useState(true);
    const fetchRequest = async () => {
        fetchFunction()
            .then(res => {
                setData([...data, ...res[0]])
                setHasMore(res[1])
            });
    }

    return [data, fetchRequest, hasMore]
}

export default useFetchPadding